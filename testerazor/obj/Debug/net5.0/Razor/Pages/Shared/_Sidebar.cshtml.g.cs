#pragma checksum "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\Shared\_Sidebar.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "eb1b41bd6fffeec650de656742de7840f5ae817a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Pages_Shared__Sidebar), @"mvc.1.0.view", @"/Pages/Shared/_Sidebar.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\_ViewImports.cshtml"
using testerazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\_ViewImports.cshtml"
using testerazor.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"eb1b41bd6fffeec650de656742de7840f5ae817a", @"/Pages/Shared/_Sidebar.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b5696b4dc8bd0ee000fdb5ce0e02268e771facb0", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Shared__Sidebar : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/adminlte/dist/img/AdminLTELogo.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("FirstChallenge Logo"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("brand-image img-circle elevation-3"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("opacity: .8"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<aside class=\"main-sidebar sidebar-dark-primary elevation-4\">\n    \n    <a");
            BeginWriteAttribute("href", " href=\"", 73, "\"", 106, 2);
#nullable restore
#line 3 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 80, ViewBag.Rooturl, 80, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 96, "/dashboard", 96, 10, true);
            EndWriteAttribute();
            WriteLiteral(" class=\"brand-link\">\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "eb1b41bd6fffeec650de656742de7840f5ae817a5117", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
        <span class=""brand-text font-weight-light"">First Challenge</span>
    </a>

    <div class=""sidebar"">
        <div class="" pb-2d-flex justify-content-center"">
        </div>

        <nav class=""mt-2"">
            <ul class=""nav nav-pills nav-sidebar flex-column"" data-widget=""treeview"" role=""menu"" data-accordion=""false"">
                <li class=""nav-item"">
                    <a");
            BeginWriteAttribute("href", " href=\"", 665, "\"", 698, 2);
#nullable restore
#line 15 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 672, ViewBag.Rooturl, 672, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 688, "/dashboard", 688, 10, true);
            EndWriteAttribute();
            WriteLiteral(@" class=""nav-link"" id=""da"">
                        <i class=""nav-icon fas fa-tachometer-alt""></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class=""nav-item"" data-tree="""">
                    <a href=""#"" class=""nav-link"" id=""tc"">
                        <i class=""nav-icon fas fa-users""></i>
                        <p>
                            Clientes
                            <i class=""right fas fa-angle-left""></i>
                        </p>
                    </a>
                    <ul class=""nav nav-treeview"">
                        <li class=""nav-item"">
                            <a");
            BeginWriteAttribute("href", " href=\"", 1423, "\"", 1455, 2);
#nullable restore
#line 32 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 1430, ViewBag.Rooturl, 1430, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1446, "/clientes", 1446, 9, true);
            EndWriteAttribute();
            WriteLiteral(@" class=""nav-link"" id=""lc"">
                                <i class=""far fa-circle nav-icon ""></i>
                                <p>Lista de Clientes</p>
                            </a>
                        </li>
                        <li class=""nav-item"">
                            <a");
            BeginWriteAttribute("href", " href=\"", 1751, "\"", 1801, 2);
#nullable restore
#line 38 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 1758, ViewBag.Rooturl, 1758, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1774, "/clientes/adicionar_cliente", 1774, 27, true);
            EndWriteAttribute();
            WriteLiteral(@" class=""nav-link"" id=""ac"">
                                <i class=""far fa-circle nav-icon""></i>
                                <p>Adicionar cliente</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class=""nav-item"" data-tree="""">
                    <a href=""#"" class=""nav-link"" id=""tu"">
                        <i class=""nav-icon fas fa-user-tie""></i>
                        <p>
                            Usuários
                            <i class=""right fas fa-angle-left""></i>
                        </p>
                    </a>
                    <ul class=""nav nav-treeview"">
                        <li class=""nav-item"">
                            <a");
            BeginWriteAttribute("href", " href=\"", 2555, "\"", 2587, 2);
#nullable restore
#line 55 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 2562, ViewBag.Rooturl, 2562, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2578, "/usuarios", 2578, 9, true);
            EndWriteAttribute();
            WriteLiteral(@" class=""nav-link"" id=""lu"">
                                <i class=""far fa-circle nav-icon""></i>
                                <p>Lista de Usuários</p>
                            </a>
                        </li>
                        <li class=""nav-item"">
                            <a");
            BeginWriteAttribute("href", " href=\"", 2882, "\"", 2932, 2);
#nullable restore
#line 61 "C:\Users\EnivixLabs01\source\repos\testerazor\testerazor\Pages\Shared\_Sidebar.cshtml"
WriteAttributeValue("", 2889, ViewBag.Rooturl, 2889, 16, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2905, "/usuarios/adicionar_usuario", 2905, 27, true);
            EndWriteAttribute();
            WriteLiteral(@" class=""nav-link"" id=""au"">
                                <i class=""far fa-circle nav-icon""></i>
                                <p>Adicionar usuário</p>
                            </a>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </nav>
    </div>
</aside>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
