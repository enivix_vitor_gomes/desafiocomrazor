using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Models;
using General;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace testerazor.Pages.Usuarios
{
    [Authorize]
    public class Editar_usuarioModel : PageModel
    {
        private readonly AppDbContext _context;

        public Editar_usuarioModel(AppDbContext context)
        {
            _context = context;
        }

        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o nome!")]
        [BindProperty(SupportsGet = true)]
        public string Nome { get; set; }

        [BindProperty(SupportsGet = true)]
        public string Apelido { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o email!")]
        [BindProperty(SupportsGet = true)]
        public string Email { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar a senha!")]
        [BindProperty(SupportsGet = true)]
        public string Senha { get; set; }

        [BindProperty(SupportsGet = true)]
        public long Telefone { get; set; }

        private Usuario user { get; set; }

        public List<Usuario> Usuarios { get; set; }

        public async Task OnGet()
        {
            Usuarios = new List<Usuario>();
            Usuarios = await _context.Usuarios.ToListAsync();

            foreach (var usuario in Usuarios)
            {
                if (usuario.Id == Id)
                {
                    Id = usuario.Id;
                    Nome = usuario.NomeCompleto;
                    Apelido = usuario.Apelido;
                    Email = usuario.Email;
                    Senha = PasswordService.Base64Decode(usuario.Senha);
                    Telefone = usuario.Telefone;
                }
            }
        }

        public async Task<IActionResult> OnPost()
        {
            user = new Usuario()
            {
                Id = Id,
                NomeCompleto = Nome,
                Apelido = Apelido,
                Email = Email,
                Senha = Senha,
                Telefone = Telefone
            };

            user.Senha = PasswordService.Base64Encode(user.Senha);

            try
            {
                _context.Entry(user).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return new JsonResult(new { Msg = "Falha ao alterar usuario, tente novamente!" });

            }

            return new JsonResult(new { Msg = "Usuario alterado com sucesso!", url = "https://localhost:44324/usuarios" });
        }

        public async Task<IActionResult> OnGetRemover()
        {
            var user = await _context.Usuarios.FindAsync(Id);

            if (user == null)
            {
                return new JsonResult(new { Msg = "Falha ao deletar, usuario n�o encontrado!" });
            }

            try
            {
                _context.Usuarios.Remove(user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return new JsonResult(new { Msg = "Falha ao deletar usuario, tente novamente!" });

            }
            return new JsonResult(new { Msg = "Usuario deletado com sucesso!", url = "https://localhost:44324/usuarios" });
        }
    }
}
