﻿function SucessoAdd(data) {
    Swal.fire({
        icon: "success",
        title: "Sucesso",
        text: data.msg,
        showConfirmButton: false,
        timer: 2000
    }).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
            window.location.replace(data.url)
        }
    })
}

function FalhaAdd(data) {
    Swal.fire({
        icon: "error",
        title: "Falha",
        text: data.msg,
        showConfirmButton: false,
        timer: 2000
    })
}

$(".btn-delete-u").click(function () {
    Swal.fire({
        title: 'Voce tem certeza?',
        text: "Voce nao podera reverter isso!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
        if (result.isConfirmed) {
            var url = "https://localhost:44324/Usuarios/Editar_usuario?Id=" + $(this).data("id") + "&handler=Remover";
            $.ajax({
                url: url,
                success: function (data) {
                    SucessoAdd(data);
                },
                failure: function (data) {
                    FalhaAdd(data);
                }
            });
        }
    })
});

$(".btn-delete-c").click(function () {
    Swal.fire({
        title: 'Voce tem certeza?',
        text: "Voce nao podera reverter isso!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, deletar!'
    }).then((result) => {
        if (result.isConfirmed) {
            var url = "https://localhost:44324/Clientes/Editar_cliente?Id=" + $(this).data("id") + "&handler=Remover";
            $.ajax({
                url: url,
                success: function (data) {
                    SucessoAdd(data);
                },
                failure: function (data) {
                    FalhaAdd(data);
                }
            });
        }
    })
});
