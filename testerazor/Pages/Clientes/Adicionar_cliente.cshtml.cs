using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace testerazor.Pages.Clientes
{
    [Authorize]
    public class Adicionar_clienteModel : PageModel
    {
        private readonly AppDbContext _context;

        [Required(ErrorMessage = "� obrigat�rio informar a raz�o social!")]
        [BindProperty(SupportsGet = true)]
        public string RazaoSocial { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o nome fantasia!")]
        [BindProperty(SupportsGet = true)]
        public string NomeFantasia { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o CNPJ!")]
        [BindProperty(SupportsGet = true)]
        public long CNPJ { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o logradouro!")]
        [BindProperty(SupportsGet = true)]
        public string Logradouro { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o numero!")]
        [BindProperty(SupportsGet = true)]
        public int Numero { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o bairro!")]
        [BindProperty(SupportsGet = true)]
        public string Bairro { get; set; }

        [BindProperty(SupportsGet = true)]
        public string Complemento { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o municipio!")]
        [BindProperty(SupportsGet = true)]
        public string Municipio { get; set; }
        
        [Required(ErrorMessage = "� obrigat�rio informar o CEP!")]
        [BindProperty(SupportsGet = true)]
        public long CEP { get; set; }


        private Cliente client { get; set; }

        public Adicionar_clienteModel(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            client = new Cliente()
            {
                RazaoSocial = RazaoSocial,
                NomeFantasia = NomeFantasia,
                Cnpj = CNPJ,
                Logradouro = Logradouro,
                Numero = Numero,
                Bairro = Bairro,
                Complemento = Complemento,
                Municipio = Municipio,
                CEP = CEP
            };


            try
            {
                await _context.Clientes.AddAsync(client);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return new JsonResult(new { Msg = "Falha ao cadastrar cliente, tente novamente!" });
            }

            return new JsonResult(new { Msg = "Cliente cadastrado com sucesso!", url = "https://localhost:44324/clientes" });
        }
    }
}
