﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models
{
    [Index(nameof(Cnpj), IsUnique = true)]
    [Index(nameof(RazaoSocial), IsUnique = true)]
    public class Cliente
    {
        public int Id { get; set; }
        [Required]
        public string RazaoSocial { get; set; }
        [Required]
        public string NomeFantasia { get; set; }
        [Required]
        public long Cnpj { get; set; }
        [Required]
        public string Logradouro { get; set; }
        [Required]
        public int Numero { get; set; }
        [Required]
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        [Required]
        public string Municipio { get; set; }
        [Required]
        public long CEP { get; set; }
    }
}
