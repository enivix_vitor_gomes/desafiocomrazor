using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Models;
using General;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace testerazor.Pages.Usuarios
{
    [Authorize]
    public class Adicionar_usuarioModel : PageModel
    {
        private readonly AppDbContext _context;

        
        [Required(ErrorMessage = "� obrigat�rio informar o nome!")]
        [BindProperty(SupportsGet = true)]
        public string Nome { get; set; }

        [BindProperty(SupportsGet = true)]
        public string Apelido { get; set; }
        
        [Required(ErrorMessage = "� obrigat�rio informar o email!")]
        [BindProperty(SupportsGet = true)]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "� obrigat�rio informar a senha!")]
        [BindProperty(SupportsGet = true)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [BindProperty(SupportsGet = true)]
        public string Telefone { get; set; }

        private Usuario user { get; set; }

        public Adicionar_usuarioModel(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            user = new Usuario() { 
                NomeCompleto = Nome,
                Apelido = Apelido,
                Email = Email,
                Senha = Senha,
                Telefone = long.Parse(Telefone)
            };

            user.Senha = PasswordService.Base64Encode(user.Senha);

            try
            {
                await _context.Usuarios.AddAsync(user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return new JsonResult(new { Msg = "Falha ao cadastrar usuario, tente novamente!"});
            }
            
            return new JsonResult(new { Msg = "Usuario cadastrado com sucesso!", url = "https://localhost:44324/usuarios" });
        }
    }
}
