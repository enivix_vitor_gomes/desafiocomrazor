using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace testerazor.Pages.Clientes
{
    [Authorize]
    public class Editar_clienteModel : PageModel
    {
        private readonly AppDbContext _context;

        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar a raz�o social!")]
        [BindProperty(SupportsGet = true)]
        public string RazaoSocial { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o nome fantasia!")]
        [BindProperty(SupportsGet = true)]
        public string NomeFantasia { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o CNPJ!")]
        [BindProperty(SupportsGet = true)]
        public long CNPJ { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o logradouro!")]
        [BindProperty(SupportsGet = true)]
        public string Logradouro { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o numero!")]
        [BindProperty(SupportsGet = true)]
        public int Numero { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o bairro!")]
        [BindProperty(SupportsGet = true)]
        public string Bairro { get; set; }

        [BindProperty(SupportsGet = true)]
        public string Complemento { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o municipio!")]
        [BindProperty(SupportsGet = true)]
        public string Municipio { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar o CEP!")]
        [BindProperty(SupportsGet = true)]
        public long CEP { get; set; }


        private Cliente client { get; set; }
        private List<Cliente> Clientes { get; set; }

        public Editar_clienteModel(AppDbContext context)
        {
            _context = context;
        }

        public async Task OnGet()
        {
            Clientes = new List<Cliente>();
            Clientes = await _context.Clientes.ToListAsync();

            foreach (var cliente in Clientes)
            {
                if (cliente.Id == Id)
                {
                    Id = cliente.Id;
                    RazaoSocial = cliente.RazaoSocial;
                    NomeFantasia = cliente.NomeFantasia;
                    CNPJ = cliente.Cnpj;
                    Logradouro = cliente.Logradouro;
                    Numero = cliente.Numero;
                    Bairro = cliente.Bairro;
                    Complemento = cliente.Complemento;
                    Municipio = cliente.Municipio;
                    CEP = cliente.CEP;
                }
            }
        }

        public async Task<IActionResult> OnPost()
        {
            client = new Cliente()
            {
                Id = Id,
                RazaoSocial = RazaoSocial,
                NomeFantasia = NomeFantasia,
                Cnpj = CNPJ,
                Logradouro = Logradouro,
                Numero = Numero,
                Bairro = Bairro,
                Complemento = Complemento,
                Municipio = Municipio,
                CEP = CEP
            };

            try
            {
                _context.Entry(client).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return new JsonResult(new { Msg = "Falha ao alterar cliente, tente novamente!" });

            }

            return new JsonResult(new { Msg = "Cliente alterado com sucesso!", url = "https://localhost:44324/clientes" });
        }

        public async Task<IActionResult> OnGetRemover()
        {
            var Client = await _context.Clientes.FindAsync(Id);

            if (Client == null)
            {
                return new JsonResult(new { Msg = "Falha ao deletar, cliente n�o encontrado!" });
            }

            try
            {
                _context.Clientes.Remove(Client);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return new JsonResult(new { Msg = "Falha ao deletar cliente, tente novamente!" });

            }
            return new JsonResult(new { Msg = "Cliente deletado com sucesso!", url = "https://localhost:44324/clientes" });
        }
    }
}
