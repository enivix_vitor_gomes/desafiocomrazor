using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace testerazor.Pages.Clientes
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _context;
        public List<Cliente> Clientes { get; set; }

        public IndexModel(AppDbContext context)
        {
            _context = context;
        }

        public async Task OnGet()
        {
            Clientes = new List<Cliente>();

            Clientes = await _context.Clientes.ToListAsync();
        }
    }
}
