using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Domain.Models;
using Domain.Context;
using Microsoft.EntityFrameworkCore;

namespace testerazor.Pages.Usuarios
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _context;
        public List<Usuario> Usuarios { get; set; }

        public IndexModel(AppDbContext context)
        {
            _context = context;
        }

        public async Task OnGet()
        {
            Usuarios = new List<Usuario>();

            Usuarios = await _context.Usuarios.ToListAsync();
        }
    }
}
