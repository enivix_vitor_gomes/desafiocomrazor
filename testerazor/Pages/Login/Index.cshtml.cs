using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Domain.Context;
using Microsoft.EntityFrameworkCore;
using General;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

namespace testerazor.Pages.Login
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _context;

        public IndexModel(AppDbContext context)
        {
            _context = context;
        }

        [Required(ErrorMessage = "� obrigat�rio informar o email!")]
        [BindProperty(SupportsGet = true)]
        public string Email { get; set; }

        [Required(ErrorMessage = "� obrigat�rio informar a senha!")]
        [BindProperty(SupportsGet = true)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {

            var user = await _context.Usuarios.FirstOrDefaultAsync(x => x.Email == Email);

            if (user == null)
            {
                return new JsonResult(new { message = "Email ou senha inv�lidos" });
            }

            if (!PasswordService.CompararSenhas(user.Senha, Senha))
            {
                return new JsonResult(new { message = "Email ou senha inv�lidos" });
            }
            else
            {
                int usuarioId = user.Id;
                string nome = user.Apelido;

                List<Claim> direitosAcesso = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, usuarioId.ToString()),
                    new Claim(ClaimTypes.Name, nome)
                };

                var identity = new ClaimsIdentity(direitosAcesso, "Identity.Login");
                var userPrincipal = new ClaimsPrincipal(new[] { identity });

                await HttpContext.SignInAsync(userPrincipal,
                    new AuthenticationProperties
                    {
                        IsPersistent = false,
                        ExpiresUtc = DateTime.Now.AddHours(1)
                    });

                return RedirectToAction("Index", "Dashboard");
            }

            //var token = TokenService.GenerateToken(user);
        }

        public IActionResult OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Dashboard");
            }

            return null;
        }

        public async Task<IActionResult> OnGetLogout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync();
            }
            return RedirectToAction("Index", "Login");
        }
    }
}
