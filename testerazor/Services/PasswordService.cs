using System.Security.Cryptography;
using System.Text;

namespace General
{
    public static class PasswordService
    {
        public static string Base64Encode(string plainText)
        {
            var process1 = System.Text.Encoding.UTF8.GetBytes(plainText);
            var result1 = System.Convert.ToBase64String(process1);
                
            var process2 = System.Text.Encoding.UTF8.GetBytes(result1);
            var result2 = System.Convert.ToBase64String(process2);    

            var process3 = System.Text.Encoding.UTF8.GetBytes(result2);
            var result3 = System.Convert.ToBase64String(process3);   
            
            var process4 = System.Text.Encoding.UTF8.GetBytes(result3);
            var result4 = System.Convert.ToBase64String(process4);

            return result4;
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var process1 = System.Convert.FromBase64String(base64EncodedData);
            var result1 = System.Text.Encoding.UTF8.GetString(process1);

            var process2 = System.Convert.FromBase64String(result1);
            var result2 = System.Text.Encoding.UTF8.GetString(process2);

            var process3 = System.Convert.FromBase64String(result2);
            var result3 = System.Text.Encoding.UTF8.GetString(process3);

            var process4 = System.Convert.FromBase64String(result3);
            var result4 = System.Text.Encoding.UTF8.GetString(process4);

            return result4;
        }

        public static bool CompararSenhas(string dbstring, string input)
        {
            if (Base64Decode(dbstring) == input)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}